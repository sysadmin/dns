// Ensure that we use some reasonable defaults for our domains
DEFAULTS(
    DefaultTTL(1800),
    NAMESERVER_TTL(1800)
);

// Dummy registrar to permit verification that details such as nameservers are correct
var REG_MONITOR = NewRegistrar("dohdefault");

// CloudNS provider accounts...
var DNS_CLOUDNS_D = NewDnsProvider("cloudns-ddos");
var DNS_CLOUDNS_P = NewDnsProvider("cloudns-premium");

// deSEC.io
var DNS_DESEC_IO = NewDnsProvider("desec");

// Cloudflare
var DNS_CLOUDFLARE = NewDnsProvider("cloudflare");

// Helper metadata for Cloudflare - Record level
var CF_PROXY_OFF = {"cloudflare_proxy": "off"};     // Proxy disabled.
var CF_PROXY_ON = {"cloudflare_proxy": "on"};       // Proxy enabled.

// Helper records for Cloudflare - Domain level
var CF_PROXY_DEFAULT_OFF = {"cloudflare_proxy_default": "off"}; // Proxy disabled (default)
var CF_PROXY_DEFAULT_ON = {"cloudflare_proxy_default": "on"};   // Proxy enabled
var CF_UNIVERSALSSL_OFF = { cloudflare_universalssl: "off" };   // Universal SSL disabled
var CF_UNIVERSALSSL_ON = { cloudflare_universalssl: "on" };     // Universal SSL enabled (default)

// Helper function to assist in the setup of ACME validation records
var ACME_CHALLENGE = function(the_domain) {
  return [
    CNAME("_acme-challenge", the_domain + "._challenge.local-kde.org."),
  ]
}

// Bring in the configuration for all our various domains now
require_glob('./domains/');

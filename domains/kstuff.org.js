D("kstuff.org",
    REG_MONITOR,
    DnsProvider(DNS_CLOUDNS_P),
    ACME_CHALLENGE("kstuff.org"),

    A("@", "49.12.122.15"),
    AAAA("@", "2a01:4f8:242:53eb::4"),
    MX("@", 10, "letterbox.kde.org."),

    CNAME("www", "tinami.kde.org."),
    CNAME("data", "tinami.kde.org.")
)

D("kde.org",
    REG_MONITOR,
    DnsProvider(DNS_CLOUDNS_D),
    ACME_CHALLENGE("kde.org"),

    // Root zone records...
    A("@", "85.10.198.55"),
    AAAA("@", "2a01:4f8:a0:600e::5"),
    MX("@", 10, "letterbox.kde.org."),

    // Email policy keys
    TXT("@", "v=spf1 mx ip4:46.43.1.242 ip6:2001:41c9:1:41e::242 ip4:209.51.188.41 ip6:2001:470:142:8::100 ?all"),

    // Domain ownership verification keys - Google
    TXT("@", "google-site-verification=tzNwQZyrp8RlOlIzlAPwBkBzDggU0cb65lXEvosYjmw"),
    TXT("@", "google-site-verification=tRZREtDgdCVFfISwRBQA96Zs3Nt5V0ilIbbhjCwvnRs"),
    // Domain ownership verification keys - Microsoft Azure Entra ID
    TXT("@", "MS=ms58770837"),
    // Domain ownership verification keys - DNSWL.org
    TXT("_token._dnswl", "bqeo9iwyo2atbqisqh2r38pj4j736rmg"),
    // Domain ownership verification keys - Cloudflare
    TXT("cloudflare-verify", "416848817-181711384"),

    // Service records for KDE.org
    SRV("_xmpp-client._tcp", 5, 0, 5222, "platna.kde.org."),
    SRV("_xmpp-server._tcp", 5, 0, 5269, "platna.kde.org."),
    SRV("_jabber._tcp", 5, 0, 5269, "platna.kde.org."),
    SRV("_matrix._tcp", 10, 5, 443, "kde.modular.im."),
    SRV("_matrix._tcp.im", 10, 5, 8448, "im.kde.org."),

    // DKIM Mail verification keys
    TXT("default._domainkey", "v=DKIM1; g=*; k=rsa; t=y; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC+u9rA8lL8SEyi86VbJtM/avtqlkuuZAGl/4R0qr5SR30+K2kgB5NRYJrUVwJAIQQ0u66kSEX7cNDEThQaUnzgpKNNwpl0JjMbdhG/BEIdcXQ96FCA3vjOgp5WoWcauEj44G+lbWRizeMOZWhfJ5wG6muFSHQ2AgnjEBcxTidjLwIDAQAB"),
    TXT("transact._domainkey", "v=DKIM1; k=rsa; s=email; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQD5KB/7OYdZVM7ZLsfjsycqE13NCSpFCXcKJcT2cdxHXi+RaBeS/GohVCQ6dINB7O8wJAYGA4avr/n70RF8TMGBQ1GrtoEPtCn6x7aGZr0glLZIQz58mT/rNecbQj19S4iuJR8IC3zpj6OIcLrAY7AqK5M9ax8Sb8pEMHdSVCNv2wIDAQAB"),
    TXT("lists._domainkey", "v=DKIM1; h=sha256; k=rsa; s=email; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5zpgWWyTFXq4jGmggkGvohUCqPqzEceEH/AkYRSGHMiFSkE3l59eCFnwoHynT/i5u8tmvcMfCM/Igb4Y11ls1z+SE7ZL9KmiCTgYxxVWXmKJnSqiYKND6UHxushOsBK2q8TDUbgCiaEWeyNVbWqHAYzG4Vplyp98CwkjlI7l0HMLqdw4XfMISGG5FezKWKwn7xIqh0RILyzI3v9jSdouKEMra6WW55HJDeyC8xNWnx2SeRUh7OLXQmf3mPS/ARjXdeNUxyTF0EBDAApcIEBWEKNuluaJ3zAgg5R1fAt6f8WfKCiBljbUl5CBAHLYb4QM6wscvXCwHhvzq08o1qdlwwIDAQAB"),
    TXT("users._domainkey", "v=DKIM1; h=sha256; k=rsa; s=email; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAncAdJkHAX1cIIhK0C7M1nyMqWhOwKv480Z8C57kwALAnShd3pvY+8v0UykQCIodwBVlihHX7PXq7V8kWvF436aieGHn/CZOs1Y+PM+E7n/buaJbjJqnIxG8Ygt+AN2ws7D+65OygTJVyqF4qpkzjXSQ9y46x2f/E54HydnIw8bUw0xAnLBVnaphux/WV/0vBYhGeT5hK71POeXbtTHsFxDAaaoXVSLpWdcMbd3zSwCo6Kn8IvT1tZWgcpmGwI/I0Ez7Uhzr7URbcYg/P69Wa/UaWj4ZixY8zEPliWF6cG/gL0uAShOc1B4KePyQHn7t9sdL9vZI+LVV7pPKs2c6K0QIDAQAB"),
    TXT("accounts._domainkey", "v=DKIM1; h=sha256; k=rsa; s=email; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0KfupjMcHWmrfm9DTx1Dh/Sl4cdFIDM/kNA+Xgt43hS0qfp8qi0XQmxK3kE6Aua5ugKaEWh0IYu7GJpdan7gNH7BISLK9ZCbXDg2LNTr+AHaO7OB4dk5KZUFo7b1BGrjN16RKHbMSDr0FJ1HRzabzXe8Ix9ecyapFKH/K/PFMU2mwth1aMicScl1zhk5vo5mpP/b8vUfcUZN6eMgjcStcm+TUN6C1ldZ3LHTEWh9u8GR3E4nw93hPyEKz8TdLwjSNYb1u/qibhDWR9NcxSl6U296ZWHcj7wd/TpNyZJgxZUvVhRk2umPZvU/pRYqfZG1U4yTwvb8uPJYBCfkPw9s0QIDAQAB"),
    TXT("lists3._domainkey", "v=DKIM1; h=sha256; k=rsa; s=email; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAliFus4QgFBCDdekrGOsQ9DpsiYPxbjJolAZ99TpSpyuNyGyr0dE8TRvMIpfRw3O3BHLfouJ4ptIwq+ojjjmDyxcMT71zVYof7uhBMvE9RupFCFWBRPFV8NSlpmxtXd0XDbxmx08Q2+yCugZ5z2/9OyzqUamTJF8XlhNOZaRCz2zsAytqTke+BKE+Yg5DO4RE6Jj8YryeHT5yvGKH0OsumvuSE8mp4QlO0jkEiYcGuxGWOOibO51G66bjLpz3G9wOhewk2s4nefycvjTb5Ul3grae9jaBtc7z9H2zeMuCxd5M8LXIO376IMqOUJd/MUzmI/fEasVEur0HOttZYjVF3wIDAQAB"),

    // Externally operated CNAMEs
    CNAME("webchat", "kde.riot.im."),
    CNAME("matrix", "kde.modular.im."),

    // Externally operated CNAMEs, fronted by KDE Cloudflare account
    CNAME("store", "store.kde.org.cdn.cloudflare.net."),

    // Externally operated services only referenced by IP address
    A("ballot", "139.59.139.194"),
    A("unifiedpush", "159.69.246.219"),
    A("alerts", "85.215.55.234"),
    AAAA("alerts", "2a02:247a:25a:8200:1::1"),

    // Sysadmin operated servers and services follow
    // Start with machines that are at varying providers

    // Dalca - CI Build Host
    // Machine provided by Dalhousie University
    A("dalca", "192.75.96.253"),
    AAAA("dalca", "2001:410:a000:50::30"),
    MX("dalca", 10, "letterbox.kde.org."),

    // Hepta - Code search, Telemetry and Server monitoring
    // VM provided by Stepping Stone AG
    A("hepta", "185.85.125.73"),
    CNAME("lxr", "hepta.kde.org."),
    CNAME("telemetry", "hepta.kde.org."),
    CNAME("metrics", "hepta.kde.org."),

    // Sylvii - KVM Host
    // Machine provided by EuroVPS
    A("sylvii", "77.235.60.41"),
    MX("sylvii", 10, "letterbox.kde.org."),

    // Emberi - KVM Host
    // Machine provided by EuroVPS
    A("emberi", "77.235.60.42"),
    MX("emberi", 10, "letterbox.kde.org."),

    // Phasia - KVM Host
    // Machine provided by EuroVPS
    A("phasia", "77.235.60.43"),
    MX("phasia", 10, "letterbox.kde.org."),
    // Bucero - WebSVN VM
    CNAME("bucero", "phasia.kde.org."),
    CNAME("websvn", "websvn.kde.org.cdn.cloudflare.net."),
    // Ferin - Sentry VM
    CNAME("ferin", "phasia.kde.org."),
    CNAME("crash-reports", "phasia.kde.org."),
    CNAME("metadebuginfod", "phasia.kde.org."),

    // Letterbox - Primary mail server for KDE
    // VM hosted by Iomart (formerly Bytemark)
    A("letterbox", "46.43.1.242"),
    AAAA("letterbox", "2001:41c9:1:41e::242"),
    CNAME("mail", "letterbox.kde.org."),
    CNAME("lists", "letterbox.kde.org."),

    // Bluemchen
    // VM provided by GNU/FSF
    A("bluemchen", "209.51.188.41"),
    AAAA("bluemchen", "2001:470:142:8::100"),
    CNAME("devel-home", "bluemchen.kde.org."),
    CNAME("ktown", "bluemchen.kde.org."),
    CNAME("developer", "bluemchen.kde.org."),

    // Atypid
    // VM provided by GNU/FSF
    A("atypid", "209.51.188.114"),
    AAAA("atypid", "2001:470:142:5::114"),

    // Overwatch - Server monitoring
    // VM provided by Gaertner Gmbh
    A("overwatch", "217.13.79.76"),
    MX("overwatch", 10, "letterbox.kde.org."),
    CNAME("status", "overwatch.kde.org."),

    // Library - Server to build docs.kde.org
    // VM provided by Gaertner Gmbh
    A("library", "217.13.79.75"),
    MX("library", 10, "letterbox.kde.org."),
    CNAME("docs-staging", "library.kde.org."),

    // Rosetta
    // VM provided by Gaertner Gmbh
    A("rosetta", "217.13.79.74"),
    MX("rosetta", 10, "letterbox.kde.org."),

    // DigitalOcean Droplets
    // Order by Droplet age

    // Platna - Communication Services (Frankfurt)
    A("platna", "104.248.16.92"),
    AAAA("platna", "2a03:b0c0:3:e0::b5:2001"),
    CNAME("bnc", "platna.kde.org."),
    CNAME("irc-attachments", "platna.kde.org."),

    // Aranea - Identity (Frankfurt)
    A("aranea", "161.35.79.142"),
    AAAA("aranea", "2a03:b0c0:3:d0::110b:b001"),
    MX("aranea", 10, "letterbox.kde.org."),
    CNAME("identity", "aranea.kde.org."),

    // Cinclo - Discussion Services
    A("cinclo", "165.22.81.215"),
    AAAA("cinclo", "2a03:b0c0:3:d0::22b:6001"),
    MX("cinclo", 10, "letterbox.kde.org."),
    CNAME("discuss", "cinclo.kde.org."),
    CNAME("discuss-cdn", "1898395290.rsc.cdn77.org."),

    // Eoan - Krita Artists Forum
    A("eoan", "165.227.155.151"),
    AAAA("eoan", "2a03:b0c0:3:d0::1079:c001"),
    MX("eoan", 10, "letterbox.kde.org."),

    // Miran - Chat solutions (Matrix bridges)
    A("miran", "64.226.84.132"),
    AAAA("miran", "2a03:b0c0:3:d0::d2d:d001"),
    MX("miran", 10, "letterbox.kde.org."),
    CNAME("im", "miran.kde.org."),
    CNAME("im-admin", "miran.kde.org."),
    CNAME("matrix.im", "miran.kde.org."),

    // Microsoft Azure VMs
    // Order by VM age

    // Loxia - Headscale instance
    A("loxia", "4.223.88.48"),
    AAAA("loxia", "2603:1020:1001:5::8"),
    SSHFP("loxia", 1, 2, "247e7edc96ddd1ea80845751858b0a9f158001477f839e8f641e2d7264ba296e"),
    SSHFP("loxia", 4, 2, "7e01b683fb3262a4043111865f89a0ca6b57b216cf9c55ef20ec144670aaa20a"),
    CNAME("tail", "loxia.kde.org."),
    CNAME("monitor", "loxia.kde.org."),

    // Hetzner Dedicated Servers
    // Start with entries for the machine, and have CNAME entries beneath that for all services

    // Node1 - CI Build Host (Gitlab)
    A("node1.ci", "135.181.232.19"),
    AAAA("node1.ci", "2a01:4f9:3080:30e3::2"),
    MX("node1.ci", 10, "letterbox.kde.org."),

    // Node2 - CI Build Host (Gitlab)
    A("node2.ci", "135.181.232.18"),
    AAAA("node2.ci", "2a01:4f9:3080:30e0::2"),
    MX("node2.ci", 10, "letterbox.kde.org."),

    // Node3 - CI Build Host
    A("node3.ci", "65.21.22.94"),
    AAAA("node3.ci", "2a01:4f9:3080:4ed0::2"),
    MX("node3.ci", 10, "letterbox.kde.org."),

    // Node4 - CI Build Host
    A("node4.ci", "65.21.22.93"),
    AAAA("node4.ci", "2a01:4f9:3080:4ed8::2"),
    MX("node4.ci", 10, "letterbox.kde.org."),

    // Node5 - CI Build Host
    A("node5.ci", "65.21.22.92"),
    AAAA("node5.ci", "2a01:4f9:3080:4edb::2"),
    MX("node5.ci", 10, "letterbox.kde.org."),

    // Node6 - CI Build Host
    A("node6.ci", "37.27.226.49"),
    AAAA("node6.ci", "2a01:4f9:3100:1a5a::2"),
    MX("node6.ci", 10, "letterbox.kde.org."),

    // Proxmox KVM Node #1 (Bulk Storage)
    A("kvm01", "157.90.207.165"),
    AAAA("kvm01", "2a01:4f8:2191:31d1::2"),
    MX("kvm01", 10, "letterbox.kde.org."),

    // Proxmox KVM Node #2 (Compute)
    A("kvm02", "157.90.207.167"),
    AAAA("kvm02", "2a01:4f8:2191:31d0::2"),
    MX("kvm02", 10, "letterbox.kde.org."),
    A("balancer02", "157.90.207.157"),
    AAAA("balancer02", "2a01:4f8:2191:31d0::3"),
    MX("balancer02", 10, "letterbox.kde.org."),

    // Proxmox KVM Node #3 (Compute)
    A("kvm03", "157.90.207.166"),
    AAAA("kvm03", "2a01:4f8:2191:31d1::2"),
    MX("kvm03", 10, "letterbox.kde.org."),
    A("balancer03", "157.90.207.156"),
    AAAA("balancer03", "2a01:4f8:2191:31d1::3"),
    MX("balancer03", 10, "letterbox.kde.org."),

    // Proxmox Node Load Balanced Hosts
    CNAME("agm", "balancer02.kde.org."),

    // Chara - BigBlueButton dedicated server
    A("chara", "168.119.89.130"),
    AAAA("chara", "2a01:4f8:251:5293::2"),
    MX("chara", 10, "letterbox.kde.org."),
    CNAME("meet", "chara.kde.org."),
    CNAME("turn.meet", "chara.kde.org."),

    // Lerwini - Gitlab and Subversion server
    A("lerwini", "188.40.133.145"),
    AAAA("lerwini", "2a01:4f8:221:1dd0::2"),
    MX("lerwini", 10, "letterbox.kde.org."),
    CNAME("invent", "lerwini.kde.org."),
    CNAME("invent-registry", "lerwini.kde.org."),
    CNAME("svn", "lerwini.kde.org."),
    CNAME("anonsvn", "lerwini.kde.org."),

    // Neoave - Container host
    A("neoave", "49.12.122.21"),
    AAAA("neoave", "2a01:4f8:242:53eb::2"),
    MX("neoave", 10, "letterbox.kde.org."),
    // Caria - Future mail server
    A("caria", "49.12.122.20"),
    AAAA("caria", "2a01:4f8:242:53eb::3"),
    MX("caria", 10, "letterbox.kde.org."),
    // Tinami - Bulk file hosting
    A("tinami", "49.12.122.15"),
    AAAA("tinami", "2a01:4f8:242:53eb::4"),
    MX("tinami", 10, "letterbox.kde.org."),
    CNAME("upload", "tinami.kde.org."),
    CNAME("rsync", "tinami.kde.org."),
    CNAME("download", "tinami.kde.org."),
    CNAME("files", "tinami.kde.org."),
    CNAME("newstuff", "tinami.kde.org."),
    CNAME("origin.files", "tinami.kde.org."),
    CNAME("origin.download", "tinami.kde.org."),
    CNAME("cdn", "1088045785.rsc.cdn77.org."),
    CNAME("cdn.files", "1410965305.rsc.cdn77.org."),
    CNAME("cdn.download", "1009236516.rsc.cdn77.org."),
    // Rhei - Maps services for Marble and KDE Itinerary
    A("rhei", "49.12.122.12"),
    AAAA("rhei", "2a01:4f8:242:53eb::5"),
    MX("rhei", 10, "letterbox.kde.org."),
    CNAME("maps", "1464524895.rsc.cdn77.org."),

    // Rupico - Container host
    A("rupico", "85.10.198.46"),
    AAAA("rupico", "2a01:4f8:a0:600e::2"),
    MX("rupico", 10, "letterbox.kde.org."),
    // Phoeni - Web application server
    A("phoeni", "85.10.198.44"),
    AAAA("phoeni", "2a01:4f8:a0:600e::3"),
    MX("phoeni", 10, "letterbox.kde.org."),
    CNAME("bugs", "phoeni.kde.org."),
    CNAME("bugsfiles", "phoeni.kde.org."),
    CNAME("bugs-admin", "phoeni.kde.org."),
    CNAME("bugstest", "phoeni.kde.org."),
    CNAME("bugstest-files", "phoeni.kde.org."),
    CNAME("conf", "phoeni.kde.org."),
    CNAME("projects", "phoeni.kde.org."),
    CNAME("commits", "phoeni.kde.org."),
    CNAME("anongit", "phoeni.kde.org."),
    CNAME("geoip", "phoeni.kde.org."),
    CNAME("newsletter", "phoeni.kde.org."),
    CNAME("treasurechest", "phoeni.kde.org."),
    CNAME("reimbursements", "phoeni.kde.org."),
    CNAME("volunteers.akademy", "phoeni.kde.org."),
    // Ampel - Web application server (PHP only)
    A("ampel", "85.10.198.53"),
    AAAA("ampel", "2a01:4f8:a0:600e::4"),
    MX("ampel", 10, "letterbox.kde.org."),
    CNAME("br", "ampel.kde.org."),
    CNAME("blog.neon", "ampel.kde.org."),
    CNAME("labplot", "ampel.kde.org."),
    CNAME("survey", "ampel.kde.org."),
    CNAME("stats", "ampel.kde.org."),
    CNAME("news", "ampel.kde.org."),
    CNAME("wikisandbox", "ampel.kde.org."),
    CNAME("gcompris-cookbook", "ampel.kde.org."),
    CNAME("community", "community.kde.org.cdn.cloudflare.net."),
    CNAME("userbase", "userbase.kde.org.cdn.cloudflare.net."),
    CNAME("techbase", "techbase.kde.org.cdn.cloudflare.net."),
    // Tyran - Static website server
    A("tyran", "85.10.198.55"),
    AAAA("tyran", "2a01:4f8:a0:600e::5"),
    MX("tyran", 10, "letterbox.kde.org."),
    CNAME("20years", "tyran.kde.org."),
    CNAME("25years", "tyran.kde.org."),
    CNAME("25for25", "tyran.kde.org."),
    CNAME("akademy", "tyran.kde.org."),
    CNAME("akademy2006", "tyran.kde.org."),
    CNAME("akademy2007", "tyran.kde.org."),
    CNAME("akademy2008", "tyran.kde.org."),
    CNAME("akademy2009", "tyran.kde.org."),
    CNAME("akademy2010", "tyran.kde.org."),
    CNAME("akademy2012", "tyran.kde.org."),
    CNAME("apps", "tyran.kde.org."),
    CNAME("amarok", "tyran.kde.org."),
    CNAME("blogs", "tyran.kde.org."),
    CNAME("cantor", "tyran.kde.org."),
    CNAME("choqok", "tyran.kde.org."),
    CNAME("commit-digest", "tyran.kde.org."),
    CNAME("conference2004", "tyran.kde.org."),
    CNAME("conference2005", "tyran.kde.org."),
    CNAME("conference2006", "tyran.kde.org."),
    CNAME("conference2007", "tyran.kde.org."),
    CNAME("conference2008", "tyran.kde.org."),
    CNAME("conference2009", "tyran.kde.org."),
    CNAME("cutehmi", "tyran.kde.org."),
    CNAME("develop", "tyran.kde.org."),
    CNAME("docs", "tyran.kde.org."),
    CNAME("dot", "tyran.kde.org."),
    CNAME("eco", "tyran.kde.org."),
    CNAME("edu", "tyran.kde.org."),
    CNAME("elisa", "tyran.kde.org."),
    CNAME("ev", "tyran.kde.org."),
    CNAME("forum", "tyran.kde.org."),
    CNAME("kdeev", "tyran.kde.org."),
    CNAME("kde-ev", "tyran.kde.org."),
    CNAME("cat", "tyran.kde.org."),
    CNAME("extragear", "tyran.kde.org."),
    CNAME("freebsd", "tyran.kde.org."),
    CNAME("fr", "tyran.kde.org."),
    CNAME("games", "tyran.kde.org."),
    CNAME("ghostwriter", "tyran.kde.org."),
    CNAME("go", "tyran.kde.org."),
    CNAME("haruna", "tyran.kde.org."),
    CNAME("hig", "tyran.kde.org."),
    CNAME("jointhegame", "tyran.kde.org."),
    CNAME("jp", "tyran.kde.org."),
    CNAME("juk", "tyran.kde.org."),
    CNAME("kdeconnect", "tyran.kde.org."),
    CNAME("kdesrc-build", "tyran.kde.org."),
    CNAME("kdesvn-build", "tyran.kde.org."),
    CNAME("kde-builder", "tyran.kde.org."),
    CNAME("kgeotag", "tyran.kde.org."),
    CNAME("kid3", "tyran.kde.org."),
    CNAME("kmplayer", "tyran.kde.org."),
    CNAME("konsole", "tyran.kde.org."),
    CNAME("kontact", "tyran.kde.org."),
    CNAME("konversation", "tyran.kde.org."),
    CNAME("kpdf", "tyran.kde.org."),
    CNAME("kst", "tyran.kde.org."),
    CNAME("kst-plot", "tyran.kde.org."),
    CNAME("kstars", "tyran.kde.org."),
    CNAME("lakademy", "tyran.kde.org."),
    CNAME("manifesto", "tyran.kde.org."),
    CNAME("marble", "tyran.kde.org."),
    CNAME("mentorship", "tyran.kde.org."),
    CNAME("minuet", "tyran.kde.org."),
    CNAME("multimedia", "tyran.kde.org."),
    CNAME("neon", "tyran.kde.org."),
    CNAME("releases.neon", "tyran.kde.org."),
    CNAME("okular", "tyran.kde.org."),
    CNAME("networkcheck", "tyran.kde.org."),
    CNAME("notes", "tyran.kde.org."),
    CNAME("paste", "tyran.kde.org."),
    CNAME("peruse", "tyran.kde.org."),
    CNAME("planet", "tyran.kde.org."),
    CNAME("relate", "tyran.kde.org."),
    CNAME("rkward", "tyran.kde.org."),
    CNAME("season", "tyran.kde.org."),
    CNAME("simon", "tyran.kde.org."),
    CNAME("subtitlecomposer", "tyran.kde.org."),
    CNAME("timeline", "tyran.kde.org."),
    CNAME("tr", "tyran.kde.org."),
    CNAME("utils", "tyran.kde.org."),
    CNAME("vvave", "tyran.kde.org."),
    CNAME("wiki", "tyran.kde.org."),
    CNAME("www", "tyran.kde.org."),
    CNAME("zanshin", "tyran.kde.org."),
    CNAME("api", "tyran.kde.org."),
    CNAME("api-staging", "tyran.kde.org."),
    CNAME("autoconfig", "1329036395.rsc.cdn77.org."),

    // Spara - Neon services server
    A("spara", "138.201.132.177"),
    AAAA("spara", "2a01:4f8:172:2fef::2"),
    MX("spara", 10, "letterbox.kde.org."),
    CNAME("debuginfod.neon", "spara.kde.org."),
    CNAME("contents.neon", "spara.kde.org."),
    CNAME("archive-api.neon", "spara.kde.org."),
    CNAME("origin.archive.neon", "spara.kde.org."),
    CNAME("build.neon", "spara.kde.org."),
    CNAME("metadata.neon", "spara.kde.org."),
    CNAME("download.kde.internal.neon", "spara.kde.org."),
    CNAME("archive.neon", "1613612302.rsc.cdn77.org."),

    // Hersili - Container host
    A("hersili", "168.119.32.169"),
    AAAA("hersili", "2a01:4f8:242:1ed5::2"),
    MX("hersili", 10, "letterbox.kde.org."),
    // Code - Phabricator server
    A("code", "168.119.32.159"),
    AAAA("code", "2a01:4f8:242:1ed5::4"),
    MX("code", 10, "letterbox.kde.org."),
    CNAME("phabricator", "code.kde.org."),
    // Pimo - Nextcloud server
    A("pimo", "168.119.32.175"),
    AAAA("pimo", "2a01:4f8:242:1ed5::5"),
    CNAME("collaborate", "pimo.kde.org."),
    CNAME("online-editor", "pimo.kde.org."),
    CNAME("share", "pimo.kde.org."),

    // Austro - Container host
    A("austro", "168.119.90.200"),
    AAAA("austro", "2a01:4f8:251:53cf::2"),
    MX("austro", 10, "letterbox.kde.org."),
    // Hexure - Translation services server (scripty, l10n website)
    A("hexure", "168.119.90.199"),
    AAAA("hexure", "2a01:4f8:251:53cf::3"),
    MX("hexure", 10, "letterbox.kde.org."),
    CNAME("l10n", "hexure.kde.org."),
    CNAME("i18n", "hexure.kde.org."),
    CNAME("*.l10n", "hexure.kde.org."),
    CNAME("*.i18n", "hexure.kde.org."),
    CNAME("www.*.l10n", "hexure.kde.org."),
    CNAME("www.*.i18n", "hexure.kde.org."),
    // Steno
    A("steno", "168.119.90.198"),
    AAAA("steno", "2a01:4f8:251:53cf::4"),
    MX("steno", 10, "letterbox.kde.org.")
)

D("kdemail.net",
    REG_MONITOR,
    DnsProvider(DNS_CLOUDNS_P),
    ACME_CHALLENGE("kdemail.net"),

    A("@", "85.10.198.55"),
    AAAA("@", "2a01:4f8:a0:600e::5"),
    MX("@", 10, "letterbox.kde.org."),

    TXT("@", "v=spf1 mx ip4:46.43.1.242 ip6:2001:41c9:1:41e::242 ip4:208.118.235.41 ip6:2001:4830:134:8::100 ip4:209.51.188.41 ip6:2001:470:142:8::100 ?all"),

    TXT("users._domainkey", "v=DKIM1; h=sha256; k=rsa; s=email; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2MQi1SsCjCJpjDTDHuOEGBypQJidfXqv3NTuFkrNbZDWJbI1VmNFRB9Z5wDmqwo2h4cMmBmNmPrHBXAwZNAyGG1jcBeAi+60jWc/ArFGp6UV16qWKkCwTGvO0jVHd3vQXbjNOdXoebgfyjp0AgQRFadMzPo1vTAiXwunS2t69nGUYyQ5ocmoIZ20rojkMrCABYSYphSAoHejr8/Gr91n/WBqNwFb7jo6NPJ79FQIXkf04XSGRZcoo0Jbx/b3DFSNEERT99BU8Z4jSfAtPyFZ/ZHOTS1U2sqB5sxr37+J6PCXL1Ig2bVmDhj+/z0oXV2xKrL5ruFF7rdf4WNAchz0+QIDAQAB"),
    TXT("general._domainkey", "v=DKIM1; h=sha256; k=rsa; s=email; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmNvrtqGhBcXJBeGKMrt/DjrtdyF5Q+3pGW+kqggGbFfYMeSoliVpN7yhbez3mc0Faoaqp85pRZ/WF8EVU+0lGq0yv4u9gXeQzXPasaCG9Tw2wsjd/V7PcWU+tIY167ai5iK38np8MS0mPwvhan4lsVNuj+nqE5u72r7EBhBjtRD6iTE1IDr/2FBc5FPnsXxBrmItVovaaKJ7ZxH6vYQgbSWL1ATEOBzCvwDxyU3o3w9x08RNY4Lm84ExU+5wjIgjTbK/HlnxndNNCURerT6yyXZweGpVUI5qF2GirihKX05Qf9WO+CLN4pYihdIUVDc5LsXb8hLV7zAX6USkTuCWdwIDAQAB"),

    CNAME("www", "tyran.kde.org.")
)

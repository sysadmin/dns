D("rolisteam.org",
    REG_MONITOR,
    DnsProvider(DNS_CLOUDNS_P),
    ACME_CHALLENGE("rolisteam.org"),

    A("@", "85.10.198.55"),
    AAAA("@", "2a01:4f8:a0:600e::5"),
    MX("@", 10, "mx1.mail.ovh.net."),
    MX("@", 20, "mx2.mail.ovh.net."),
    MX("@", 100, "mx3.mail.ovh.net."),

    CNAME("www", "tyran.kde.org."),
    CNAME("doc", "tyran.kde.org."),
    CNAME("wiki", "tyran.kde.org."),

    A("forum", "80.15.125.140"),
    A("blog", "80.15.125.140"),
    A("test", "80.15.125.140"),
    A("nwod", "80.15.125.140"),
    A("arpege", "80.15.125.140")
)

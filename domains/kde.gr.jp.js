D("kde.gr.jp",
    REG_MONITOR,
    DnsProvider(DNS_CLOUDNS_P),
    ACME_CHALLENGE("kde.gr.jp"),

    A("@", "85.10.198.55"),
    AAAA("@", "2a01:4f8:a0:600e::5"),
    MX("@", 10, "mail.kde.gr.jp."),

    CNAME("www", "tyran.kde.org."),

    A("mail", "54.249.34.145")
)

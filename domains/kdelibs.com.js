D("kdelibs.com",
    REG_MONITOR,
    DnsProvider(DNS_CLOUDNS_P),
    ACME_CHALLENGE("kdelibs.com"),

    A("@", "85.10.198.53"),
    AAAA("@", "2a01:4f8:a0:600e::4"),
    MX("@", 10, "letterbox.kde.org."),

    CNAME("www", "ampel.kde.org.")
)

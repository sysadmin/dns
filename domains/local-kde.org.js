D("local-kde.org",
    REG_MONITOR,
    DnsProvider(DNS_DESEC_IO),
    DefaultTTL(3600),

    TXT("@", "v=spf1 -all"),
    TXT("_dmarc", "v=DMARC1; p=reject; sp=reject; adkim=s; aspf=s;"),

    TXT("*._domainkey", "v=DKIM1; p="),

    A("*", "188.40.133.145", CF_PROXY_OFF),
    AAAA("*", "2a01:4f8:221:1dd0::2", CF_PROXY_OFF)
)

D("kde.ru",
    REG_MONITOR,
    DnsProvider(DNS_CLOUDNS_P),
    ACME_CHALLENGE("kde.ru"),

    A("@", "85.10.198.55"),
    AAAA("@", "2a01:4f8:a0:600e::5"),
    MX("@", 10, "aspmx.l.google.com."),
    MX("@", 20, "alt1.aspmx.l.google.com."),
    MX("@", 20, "alt2.aspmx.l.google.com."),
    TXT("@", "v=spf1 include:aspmx.googlemail.com ~all"),

    CNAME("www", "tyran.kde.org."),
    CNAME("lists", "lists.altlinux.org.")
)

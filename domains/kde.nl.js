D("kde.nl",
    REG_MONITOR,
    DnsProvider(DNS_CLOUDFLARE),
    DefaultTTL(1),
    ACME_CHALLENGE("kde.nl"),

    MX("@", 10, "letterbox.kde.org."),
    ALIAS("@", "tyran.kde.org.", CF_PROXY_ON),

    TXT("@", "v=spf1 mx ip4:46.43.1.242 ip6:2001:41c9:1:41e::242 ip4:209.51.188.41 ip6:2001:470:142:8::100 ?all"),

    TXT("users._domainkey", "v=DKIM1; h=sha256; k=rsa; s=email; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzV7hoqeWmafXsk3IApJNd8W7fDouzr8kSOpvO/mNsIZqM6BHH/fNRE/XEHcSLfcuB9UfkhSjEgXdKMpnVJmW+NG4JeGjPxE0VpIdzTknS+hZYLj51cfAHiPkedbiVVLvikD/whkF53LjU6bSJhaEC1JfHhkLIMit9krOwtavmRJBze/QEJDynQCFww6yJ7hJ2r+iRQtCQFWI1DexB+cXRM2sWYNUESS/qTTkeRo2lZ/b+dVNgQ1VmC9tPIC/gKTdtwwZge7tWmXgQrtc2XSx2f7XJayeg/zHxUVtt17QXlA+w7ar6+IzKTgz8LgmwodPIEsz9heTpH5OSIeDDppNjQIDAQAB"),

    CNAME("www", "tyran.kde.org.", CF_PROXY_ON)
)

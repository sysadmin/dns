D("kdetalk.net",
    REG_MONITOR,
    DnsProvider(DNS_CLOUDNS_P),
    ACME_CHALLENGE("kdetalk.net"),

    A("@", "104.248.16.92"),
    AAAA("@", "2a03:b0c0:3:e0::b5:2001"),

    SRV("_jabber._tcp", 0, 0, 5269, "platna.kde.org."),
    SRV("_xmpp-server._tcp", 0, 0, 5269, "platna.kde.org."),
    SRV("_xmpp-client._tcp", 0, 0, 5222, "platna.kde.org."),

    CNAME("www", "platna.kde.org."),
    CNAME("conference", "platna.kde.org."),
    CNAME("proxy", "platna.kde.org.")

)

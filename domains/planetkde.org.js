D("planetkde.org",
    REG_MONITOR,
    DnsProvider(DNS_CLOUDNS_P),
    ACME_CHALLENGE("planetkde.org"),

    A("@", "85.10.198.55"),
    AAAA("@", "2a01:4f8:a0:600e::5"),
    MX("@", 10, "letterbox.kde.org."),

    CNAME("www", "tyran.kde.org."),
    CNAME("pim", "tyran.kde.org."),
    CNAME("fr", "tyran.kde.org."),
    CNAME("es", "tyran.kde.org."),
    CNAME("br", "tyran.kde.org."),
    CNAME("pt", "tyran.kde.org."),
    CNAME("pl", "tyran.kde.org."),
    CNAME("it", "tyran.kde.org."),
    CNAME("zh", "tyran.kde.org.")
)

var KONQUEROR_RECORDS = [
    A("@", "85.10.198.55"),
    AAAA("@", "2a01:4f8:a0:600e::5"),
    MX("@", 10, "letterbox.kde.org."),

    CNAME("www", "tyran.kde.org.")
];

D("konqueror.org", REG_MONITOR, DnsProvider(DNS_CLOUDNS_P), ACME_CHALLENGE("konqueror.org"), KONQUEROR_RECORDS);
D("konqueror.com", REG_MONITOR, DnsProvider(DNS_CLOUDNS_P), ACME_CHALLENGE("konqueror.com"), KONQUEROR_RECORDS);

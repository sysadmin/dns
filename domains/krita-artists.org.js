var KRITA_ARTISTS_RECORDS = [
    A("@", "165.227.155.151"),
    AAAA("@", "2a03:b0c0:3:d0::1079:c001"),
    MX("@", 10, "letterbox.kde.org."),

    TXT("@", "v=spf1 mx ip4:46.43.1.242 ip6:2001:41c9:1:41e::242 ip4:209.51.188.41 ip6:2001:470:142:8::100 ip4:78.47.89.111 ip6:2a01:4f8:c2c:acc7::1 -all"),
    TXT("_dmarc", "v=DMARC1; p=quarantine"),

    TXT("transact._domainkey", "v=DKIM1; k=rsa; s=email; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDaNi6iCMPpd/XCEaqrG3fDWVSyT2PaJ8d+htxLxCPd/05ek87rufPSCp//I+adBzM6gAb2d9k/Yf55E0WA/GK8YkQ2hto/xTIS0XX4NszMW3Hwl80M93oJMgWJRYfNa/kCs+znazyjZqbGTTjDJCuUb/7K+TNQW6w1+BPHGTa05wIDAQAB"),
    TXT("mail._domainkey", "v=DKIM1; h=sha256; k=rsa; s=email; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1Ravap2qbTmLEPX3ZGqgUKYkzH9Qv1DNQ9ODlI4cJcOkl0opLFMcCWArZ9aS2EwH4J08H3gCDukqFka5g2NOn1BnTS68wJupk5hHGISSipO6gHpJPYH8IM2yIUTOt5lWa4n9wGSO9RZI9wqHBEL5QivGK9+0g925/Bf/dLS3nsVVSvYTs7roeH2S0gI1dPIiv1tsD+1Xn0j4zjM2tXW7ZL6+JQMRw7BQtWU5KJtyD/edqkyImiZLo156JCJqkkPhPfIeo4NkMRe3t4iKbFs2dQ9lqt0mGkB4NKWZBnoftVy7zkWPHlgp1IE3JJ1OcNpdl40puKlpq/iqt4bUZuzyswIDAQAB"),

    TXT("@", "google-site-verification=2td2sM4L2kW6G3GrfABXtd7EgjIQK4LldHTQ60ISKp0"),

    CNAME("www", "eoan.kde.org.")
];

D("krita-artists.org", REG_MONITOR, DnsProvider(DNS_CLOUDNS_P), ACME_CHALLENGE("krita-artists.org"), KRITA_ARTISTS_RECORDS);
D("kritaartists.org", REG_MONITOR, DnsProvider(DNS_CLOUDNS_P), ACME_CHALLENGE("kritaartists.org"), KRITA_ARTISTS_RECORDS);

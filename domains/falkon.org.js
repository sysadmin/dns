D("falkon.org",
    REG_MONITOR,
    DnsProvider(DNS_CLOUDNS_P),
    ACME_CHALLENGE("falkon.org"),

    A("@", "85.10.198.55"),
    AAAA("@", "2a01:4f8:a0:600e::5"),
    MX("@", 10, "letterbox.kde.org."),

    CNAME("www", "tyran.kde.org."),
    A("store", "206.189.248.28")
)

var BEHINDKDE_RECORDS = [
    A("@", "85.10.198.55"),
    AAAA("@", "2a01:4f8:a0:600e::5"),
    MX("@", 10, "letterbox.kde.org."),

    CNAME("www", "tyran.kde.org."),
    CNAME("archive", "proxy.kovoks.nl.")
];

D("behindkde.com", REG_MONITOR, DnsProvider(DNS_CLOUDNS_P), ACME_CHALLENGE("behindkde.com"), BEHINDKDE_RECORDS)
D("behindkde.org", REG_MONITOR, DnsProvider(DNS_CLOUDNS_P), ACME_CHALLENGE("behindkde.org"), BEHINDKDE_RECORDS)

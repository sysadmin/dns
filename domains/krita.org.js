D("krita.org",
    REG_MONITOR,
    DnsProvider(DNS_CLOUDNS_D),
    ACME_CHALLENGE("krita.org"),

    A("@", "85.10.198.55"),
    AAAA("@", "2a01:4f8:a0:600e::5"),
    MX("@", 10, "letterbox.kde.org."),

    TXT("@", "v=spf1 mx ip4:46.43.1.242 ip6:2001:41c9:1:41e::242 ip4:208.118.235.41 ip6:2001:4830:134:8::100 ?all"),

    TXT("general._domainkey", "v=DKIM1; h=sha256; k=rsa; s=email; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAw+SiaGXZgDMTdXx2i4dFlH8zsHGWHNNNhOLCj2kRFdDcR1xeQ08eZnoWYCe9JxRDVr3KVefr0ipAuRxxNlzjdE0eLWtaSzQgO0eOPpgJkl+GeUA3cNFyZ/zA+gimWRRS9ZttrRcnhrFY3DerMRUxnsJW7kBS/l1Bm7taGBCDn5nYlZBNAFrx0x4BIc1ZUHEDQRHPHIKx8p9KKSyQtQXoaNKGQQHkGAVnjJFyeGWWi4FUMCS6X2278a6ocSVKJVaYgWFdBQsT00Nk35BJ81lQMwjJ1ZRmKtA28jDC85ceobIogUbGF7z+BiVr+eWu8+J+RigOjE+wTCwokGPaxWDPaQIDAQAB"),
    TXT("lists._domainkey", "v=DKIM1; h=sha256; k=rsa; s=email; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxL0CN8uEpDIhnjIbkyO1VPifLbJHrcjJBEXjkyE0XvKZTwIiPMgTe3vr7IdWvzF+qTQEDWF/cetobgZbS9hwXReIdfefEjIbNKZIwpa5847K3pl6fk2VtC+CVKxA8KzNnrLI3iOdhIgZz6XFMWUyesnrnBIkjqMYWl0aiHQZCE0vGGIlz9zR5//s1kAoHLr0xVEmFgudmaKj269UtLYQ8Fi5B8yyDNgqcE9pVnpMZCOWtuBDIhwdBKPw9e3xw772mlNgjD9PKQ7wKFP+juG3XzyCjqyr0PaOzDjjarUd7g2q1e0fQ5RnXUnemSI3w6lUQBuBztBmvQcuQSZXtA+l0QIDAQAB"),

    CNAME("www", "tyran.kde.org."),
    CNAME("docs", "tyran.kde.org."),
    CNAME("fund", "phoeni.kde.org."),
    CNAME("jp", "tyran.kde.org."),
    CNAME("scripting", "tyran.kde.org.")
);

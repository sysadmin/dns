var GLAXNIMATE_RECORDS = [
    A("@", "85.10.198.55"),
    AAAA("@", "2a01:4f8:a0:600e::5"),
    MX("@", 10, "letterbox.kde.org."),

    CNAME("www", "tyran.kde.org."),
    CNAME("docs", "tyran.kde.org.")
];

D("glaxnimate.org", REG_MONITOR, DnsProvider(DNS_CLOUDNS_P), ACME_CHALLENGE("glaxnimate.org"), GLAXNIMATE_RECORDS);
D("glaxnimate.com", REG_MONITOR, DnsProvider(DNS_CLOUDNS_P), ACME_CHALLENGE("glaxnimate.com"), GLAXNIMATE_RECORDS);
D("glaxnimate.art", REG_MONITOR, DnsProvider(DNS_CLOUDNS_P), ACME_CHALLENGE("glaxnimate.art"), GLAXNIMATE_RECORDS);

D("kdenlive.org",
    REG_MONITOR,
    DnsProvider(DNS_CLOUDNS_P),
    ACME_CHALLENGE("kdenlive.org"),

    A("@", "85.10.198.53"),
    AAAA("@", "2a01:4f8:a0:600e::4"),
    MX("@", 10, "letterbox.kde.org."),

    TXT("@", "v=spf1 mx ip4:46.43.1.242 ip6:2001:41c9:1:41e::242 ip4:208.118.235.41 ip6:2001:4830:134:8::100 ip4:209.51.188.41 ip6:2001:470:142:8::100 ?all"),

    TXT("overall._domainkey", "v=DKIM1; h=sha256; k=rsa; s=email; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAutfUJH9BvHkctAwbkLsO14oMRI5thuw2AxMgw9xpfvRTAuHjtdDrj2qYAn4tWNaVq5/Pkp+cbAW5cZOhF9kX67HGmw7t4UhiPG3Ch1MApEIA6RG1S2wbdkNNq/RqbN8QEQdMTxh+uWuRwGXIuR47lFCT+kOM0WQ/MhIXrkYvpRg7Xhc3/ttpNJfD3VO2I/XcL3sN5ASNJqIcqDelDk9kp/O1qTTsrHwyNVbyY5yXf9RbWiLs9Gqj6/egvMswhlpKlaCWgT8lae43O++uKY2cy0g+6FLoPbOHRVnEHKbvpbrqDkvybHJ5QdAQzc2VldffcOb19H5JBzJ1YPiXEj3a3wIDAQAB"),

    CNAME("www", "ampel.kde.org."),
    CNAME("beta", "tyran.kde.org."),
    CNAME("docs", "tyran.kde.org.")
)

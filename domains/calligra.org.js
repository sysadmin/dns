var CALLIGRA_RECORDS = [
    A("@", "85.10.198.55"),
    AAAA("@", "2a01:4f8:a0:600e::5"),
    MX("@", 10, "letterbox.kde.org."),

    CNAME("www", "tyran.kde.org."),
    CNAME("static", "tinami.kde.org.")
];

D("calligra.org", REG_MONITOR, DnsProvider(DNS_CLOUDNS_P), ACME_CHALLENGE("calligra.org"), CALLIGRA_RECORDS);
D("calligra-suite.org", REG_MONITOR, DnsProvider(DNS_CLOUDNS_P), ACME_CHALLENGE("calligra-suite.org"), CALLIGRA_RECORDS);
D("koffice.org", REG_MONITOR, DnsProvider(DNS_CLOUDNS_P), ACME_CHALLENGE("koffice.org"), CALLIGRA_RECORDS);
